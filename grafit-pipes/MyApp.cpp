﻿#include "MyApp.h"

#include <math.h>
#include <vector>

#include <array>
#include <list>
#include <tuple>
#include <imgui/imgui.h>
#include "includes/GLUtils.hpp"

CMyApp::CMyApp(void)
{
	srand((unsigned)time(NULL));
	m_camera.SetView(glm::vec3(0, 0, -10), glm::vec3(Lx/2, Ly/2, Lz/2), glm::vec3(0, 1, 0));
}

CMyApp::~CMyApp(void)
{
}

glm::vec3 CMyApp::GetSphereUV(float u, float v, float r)
{
	// origó középpontú, r sugarú gömb parametrikus alakja: http://hu.wikipedia.org/wiki/G%C3%B6mb#Egyenletek 
	// figyeljünk:	matematikában sokszor a Z tengely mutat felfelé, de nálunk az Y, tehát a legtöbb képlethez képest nálunk
	//				az Y és Z koordináták felcserélve szerepelnek
	u *= float(2 * M_PI);
	v *= float(M_PI);

	return glm::vec3(r * sin(v) * cos(u),
					r * cos(v),
					r * sin(v) * sin(u));
}
//
// egy parametrikus felület (u,v) paraméterértékekhez tartozó normálvektorának
// kiszámítását végző függvény
//
glm::vec3 CMyApp::GetSphereNormals(float u, float v)
{
	u *= float(2 * M_PI);
	v *= float(M_PI);
	return glm::vec3(sin(v) * cos(u), cos(v), sin(v) * sin(u));


}

void CMyApp::InitSphere()
{
	//struct Vertex{ glm::vec3 position; glm::vec3 normals; glm::vec2 texture; };
	std::vector<Vertex>vertices;

	for (int j = 0; j <= M; ++j)
	{
		for (int i = 0; i <= N; ++i)
		{
			float u = i / (float)N;
			float v = j / (float)M;
			vertices.push_back({ GetSphereUV(u, v, 0.5), GetSphereNormals(u, v), glm::vec2(1 - u, 1 - v) });

		}
	}

	std::vector<int> indices(N * M * 2 * 3);
	int index = 0;
	for (int j = 0; j < M; ++j)
	{
		for (int i = 0; i < N; ++i)
		{
			// minden négyszögre csináljunk kettő háromszöget, amelyek a következő 
			// (i,j) indexeknél született (u_i, v_j) paraméterértékekhez tartozó
			// pontokat kötik össze:
			// 
			// (i,j+1) C-----D (i+1,j+1)
			//         |\    |				A = p(u_i, v_j)
			//         | \   |				B = p(u_{i+1}, v_j)
			//         |  \  |				C = p(u_i, v_{j+1})
			//         |   \ |				D = p(u_{i+1}, v_{j+1})
			//         |    \|
			//   (i,j) A-----B (i+1,j)
			//
			// - az (i,j)-hez tartózó 1D-s index a VBO-ban: i+j*(N+1)
			// - az (i,j)-hez tartózó 1D-s index az IB-ben: i*6+j*6*N
			//		(mert minden négyszöghöz 2db háromszög = 6 index tartozik)
			//
			index = i * 6 + j * (6 * N);
			indices[index + 0] = (i)+(j) * (N + 1);
			indices[index + 1] = (i + 1) + (j) * (N + 1);
			indices[index + 2] = (i)+(j + 1) * (N + 1);
			indices[index + 3] = (i + 1) + (j) * (N + 1);
			indices[index + 4] = (i + 1) + (j + 1) * (N + 1);
			indices[index + 5] = (i)+(j + 1) * (N + 1);
		}
	}

	//
	// geometria definiálása (std::vector<...>) és GPU pufferekbe való feltöltése BufferData-val
	//

	// vertexek pozíciói:
	/*
	Az m_SphereVertexBuffer konstruktora már létrehozott egy GPU puffer azonosítót és a most következő BufferData hívás ezt
	1. bind-olni fogja GL_ARRAY_BUFFER target-re (hiszen m_SphereVertexBuffer típusa ArrayBuffer) és
	2. glBufferData segítségével áttölti a GPU-ra az argumentumban adott tároló értékeit

	*/
	m_SphereVertexBuffer.BufferData(vertices);

	// és a primitíveket alkotó csúcspontok indexei (az előző tömbökből) - triangle list-el való kirajzolásra felkészülve
	m_SphereIndices.BufferData(indices);

	// geometria VAO-ban való regisztrálása
	m_SphereVao.Init(
		{
			// 0-ás attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_SphereVertexBuffer GPU pufferben vannak
			{ CreateAttribute<		0,						// attribútum: 0
									glm::vec3,				// CPU oldali adattípus amit a 0-ás attribútum meghatározására használtunk <- az eljárás a glm::vec3-ból kikövetkezteti, hogy 3 darab float-ból áll a 0-ás attribútum
									0,						// offset: az attribútum tároló elejétől vett offset-je, byte-ban
									sizeof(Vertex)			// stride: a következő csúcspont ezen attribútuma hány byte-ra van az aktuálistól
								>, m_SphereVertexBuffer },
			{ CreateAttribute<1, glm::vec3, (sizeof(glm::vec3)), sizeof(Vertex)>, m_SphereVertexBuffer },
			{ CreateAttribute<2, glm::vec2, (2 * sizeof(glm::vec3)), sizeof(Vertex)>, m_SphereVertexBuffer },
		},
		m_SphereIndices
	);
}

glm::vec3 CMyApp::GetCylinderUV(float u, float v, float r)
{
	u *= - float(2 * M_PI);

	return glm::vec3(r * cos(u),
					v ,
					r * sin(u));
}
//
// egy parametrikus felület (u,v) paraméterértékekhez tartozó normálvektorának
// kiszámítását végző függvény
//
glm::vec3 CMyApp::GetCylinderNormals(float u, float v)
{
	u *= - float(2 * M_PI);
	return glm::vec3(cos(u), 0, sin(u));
}

void CMyApp::InitCylinder()
{
	//struct Vertex{ glm::vec3 position; glm::vec3 normals; glm::vec2 texture; };
	std::vector<Vertex>vertices;

	for (int j = 0; j <= M; ++j)
	{
		for (int i = 0; i <= N; ++i)
		{
			float u = i / (float)N;
			float v = j / (float)M;
			vertices.push_back({ GetCylinderUV(u, v, 0.3), GetCylinderNormals(u, v), glm::vec2(u, 1 - v) });

		}
	}

	std::vector<int> indices(N * M * 2 * 3);
	int index = 0;
	for (int j = 0; j < M; ++j)
	{
		for (int i = 0; i < N; ++i)
		{
			index = i * 6 + j * (6 * N);
			indices[index + 0] = (i)+(j) * (N + 1);
			indices[index + 1] = (i + 1) + (j) * (N + 1);
			indices[index + 2] = (i)+(j + 1) * (N + 1);
			indices[index + 3] = (i + 1) + (j) * (N + 1);
			indices[index + 4] = (i + 1) + (j + 1) * (N + 1);
			indices[index + 5] = (i)+(j + 1) * (N + 1);
		}
	}

	m_CylinderVertexBuffer.BufferData(vertices);

	// és a primitíveket alkotó csúcspontok indexei (az előző tömbökből) - triangle list-el való kirajzolásra felkészülve
	m_CylinderIndices.BufferData(indices);

	// geometria VAO-ban való regisztrálása
	m_CylinderVao.Init(
		{
			// 0-ás attribútum "lényegében" glm::vec3-ak sorozata és az adatok az m_SphereVertexBuffer GPU pufferben vannak
			{ CreateAttribute<		0,						// attribútum: 0
									glm::vec3,				// CPU oldali adattípus amit a 0-ás attribútum meghatározására használtunk <- az eljárás a glm::vec3-ból kikövetkezteti, hogy 3 darab float-ból áll a 0-ás attribútum
									0,						// offset: az attribútum tároló elejétől vett offset-je, byte-ban
									sizeof(Vertex)			// stride: a következő csúcspont ezen attribútuma hány byte-ra van az aktuálistól
								>, m_CylinderVertexBuffer },
			{ CreateAttribute<1, glm::vec3, (sizeof(glm::vec3)), sizeof(Vertex)>, m_CylinderVertexBuffer },
			{ CreateAttribute<2, glm::vec2, (2 * sizeof(glm::vec3)), sizeof(Vertex)>, m_CylinderVertexBuffer },
		},
		m_CylinderIndices
	);
}

void CMyApp::InitSkyBox()
{
	m_SkyboxPos.BufferData(
		std::vector<glm::vec3>{
		// hátsó lap
		glm::vec3(-1, -1, -1),
		glm::vec3(1, -1, -1),
		glm::vec3(1, 1, -1),
		glm::vec3(-1, 1, -1),
		// elülső lap
		glm::vec3(-1, -1, 1),
		glm::vec3(1, -1, 1),
		glm::vec3(1, 1, 1),
		glm::vec3(-1, 1, 1),
	}
	);

	// és a primitíveket alkotó csúcspontok indexei (az előző tömbökből) - triangle list-el való kirajzolásra felkészülve
	m_SkyboxIndices.BufferData(
		std::vector<int>{
			// hátsó lap
			0, 1, 2,
			2, 3, 0,
			// elülső lap
			4, 6, 5,
			6, 4, 7,
			// bal
			0, 3, 4,
			4, 3, 7,
			// jobb
			1, 5, 2,
			5, 6, 2,
			// alsó
			1, 0, 4,
			1, 4, 5,
			// felső
			3, 2, 6,
			3, 6, 7,
	}
	);

	// geometria VAO-ban való regisztrálása
	m_SkyboxVao.Init(
		{
			{ CreateAttribute<0, glm::vec3, 0, sizeof(glm::vec3)>, m_SkyboxPos },
		}, m_SkyboxIndices
	);

	// skybox texture
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	m_skyboxTexture.AttachFromFile("assets/xpos.png", false, GL_TEXTURE_CUBE_MAP_POSITIVE_X);
	m_skyboxTexture.AttachFromFile("assets/xneg.png", false, GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
	m_skyboxTexture.AttachFromFile("assets/ypos.png", false, GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
	m_skyboxTexture.AttachFromFile("assets/yneg.png", false, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
	m_skyboxTexture.AttachFromFile("assets/zpos.png", false, GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
	m_skyboxTexture.AttachFromFile("assets/zneg.png", true, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

	// a GL_TEXTURE_MAG_FILTER-t és a GL_TEXTURE_MIN_FILTER-t beállítja az AttachFromFile
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void CMyApp::InitShaders()
{
	// a shadereket tároló program létrehozása az OpenGL-hez hasonló módon:
	m_program.AttachShaders({
		{ GL_VERTEX_SHADER, "myVert.vert"},
		{ GL_FRAGMENT_SHADER, "myFrag.frag"}
	});

	// attributomok osszerendelese a VAO es shader kozt
	m_program.BindAttribLocations({
		{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		{ 1, "vs_in_norm" },			// VAO 1-es csatorna menjen a vs_in_norm-ba
		{ 2, "vs_in_tex" },				// VAO 2-es csatorna menjen a vs_in_tex-be
	});

	m_program.LinkProgram();

	// shader program rövid létrehozása, egyetlen függvényhívással a fenti három:
	m_programSkybox.Init(
		{
			{ GL_VERTEX_SHADER, "skybox.vert" },
			{ GL_FRAGMENT_SHADER, "skybox.frag" }
		},
		{
			{ 0, "vs_in_pos" },				// VAO 0-as csatorna menjen a vs_in_pos-ba
		}
	);

	m_postprocess.Init(
		{
			{ GL_VERTEX_SHADER, "postprocess.vert"},
			{ GL_FRAGMENT_SHADER, "postprocess.frag"}
		},
		{
			{ 0, "vs_in_pos"},
			{ 0, "vs_in_texcoords"}
		}
		);
}

void CMyApp::InitFrameBuffer(int width, int height) {
	if (m_frameBufferCreated) {
		glDeleteRenderbuffers(1, &m_depthBuffer);
		glDeleteTextures(1, &m_colorBuffer);
		glDeleteFramebuffers(1, &m_frameBuffer);
	}

	glGenFramebuffers(1, &m_frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);

	glGenTextures(1, &m_colorBuffer);
	glBindTexture(GL_TEXTURE_2D, m_colorBuffer);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_colorBuffer, 0);
	if (glGetError() != GL_NO_ERROR)
	{
		std::cout << "Error creating color attachment" << std::endl;
		exit(1);
	}

	glGenRenderbuffers(1, &m_depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer);

	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthBuffer);
	if (glGetError() != GL_NO_ERROR)
	{
		std::cout << "Error creating depth attachment" << std::endl;
		exit(1);
	}

	// -- Completeness check
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Incomplete framebuffer (";
		switch (status) {
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			std::cout << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			std::cout << "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED:
			std::cout << "GL_FRAMEBUFFER_UNSUPPORTED";
			break;
		}
		std::cout << ")" << std::endl;
		char ch;
		std::cin >> ch;
		exit(1);
	}

	// -- Unbind framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	m_frameBufferCreated = true;
}

std::vector<CMyApp::Direction> CMyApp::GetFreeDirections(int x, int y, int z) {
	std::cout << "edges and intersections-->" << edges[x][y][z] << "," << intersections[x+1][y][z] << std::endl;
	std::cout << "edges and intersections-->" << edges[x-1][y][z] << "," << intersections[x-1][y][z] << std::endl;
	std::cout << "edges and intersections-->" << edges[x][y][z] << "," << intersections[x][y+1][z] << std::endl;
	std::cout << "edges and intersections-->" << edges[x][y-1][z] << "," << intersections[x][y-1][z] << std::endl;
	std::cout << "edges and intersections-->" << edges[x][y][z] << "," << intersections[x][y][z+1] << std::endl;
	std::cout << "edges and intersections-->" << edges[x][y][z-1] << "," << intersections[x][y][z-1] << std::endl;
	std::vector<Direction> freeDirections;
	if (!edges[x][y][z] && !intersections[x + 1][y][z]) {
		freeDirections.push_back(RIGHT);
	}
	if (!edges[x - 1][y][z] && !intersections[x - 1][y][z]) {
		freeDirections.push_back(LEFT);
	}
	if (!edges[x][y][z] && !intersections[x][y + 1][z]) {
		freeDirections.push_back(UP);
	}
	if (!edges[x][y - 1][z] && !intersections[x][y - 1][z]) {
		freeDirections.push_back(DOWN);
	}
	if (!edges[x][y][z] && !intersections[x][y][z + 1]) {
		freeDirections.push_back(FORWARD);
	}
	if (!edges[x][y][z - 1] && !intersections[x][y][z - 1]) {
		freeDirections.push_back(BACKWARD);
	}
	return freeDirections;
}

glm::vec3 CMyApp::GetNextPos(int x, int y, int z, Shape s, Direction d) {
	glm::vec3 nextPos;
	switch (d) {
	case UP:
		if (s == SPHERE) {
			nextPos = glm::vec3(x, y, z);
		}
		else {
			nextPos = glm::vec3(x, y + 1, z);
		}
		break;
	case DOWN:
		nextPos = glm::vec3(x, y - 1, z);
		break;
	case LEFT:
		nextPos = glm::vec3(x - 1, y, z);
		break;
	case RIGHT:
		if (s == SPHERE) {
			nextPos = glm::vec3(x, y, z);
		}
		else {
			nextPos = glm::vec3(x + 1, y, z);
		}
		break;
	case FORWARD:
		if (s == SPHERE) {
			nextPos = glm::vec3(x, y, z);
		}
		else {
			nextPos = glm::vec3(x, y, z + 1);
		}
		break;
	case BACKWARD:
		nextPos = glm::vec3(x, y, z - 1);
		break;
	}
	return nextPos;
}

void CMyApp::AddSphere(int pipeNo, int x, int y, int z) {
	if (pipes[pipeNo].elems.size() > 0) {
		Element e = pipes[pipeNo].elems.back();
		if (e.shape == CYLINDER) {
			if (e.direction == DOWN)
				y += 1;
			if (e.direction == LEFT)
				x += 1;
			if (e.direction == BACKWARD)
				z += 1;
		}
	}
	std::vector<Direction> freeDirections = GetFreeDirections(x, y, z);
	if (pipes[pipeNo].cylinders.size() > 0) {
		freeDirections.erase(std::remove(freeDirections.begin(), freeDirections.end(), pipes[pipeNo].cylinders.back().direction), freeDirections.end());
	}
	if (freeDirections.size() > 1) {
		Direction newDirection = freeDirections[rand() % freeDirections.size()];
		intersections[x][y][z] = 1;
		pipes[pipeNo].spheres.push_back({ SPHERE, glm::vec3(x,y,z), newDirection, GetNextPos(x, y, z, SPHERE, newDirection), pipes[pipeNo].currentColor });
		pipes[pipeNo].elems.push_back({ SPHERE, glm::vec3(x,y,z), newDirection, GetNextPos(x, y, z, SPHERE, newDirection), pipes[pipeNo].currentColor });
	} else if (freeDirections.size() == 1) {
		Direction newDirection = freeDirections[0];
		intersections[x][y][z] = 1;
		pipes[pipeNo].spheres.push_back({ SPHERE, glm::vec3(x,y,z), newDirection, GetNextPos(x, y, z, SPHERE, newDirection), pipes[pipeNo].currentColor });
		pipes[pipeNo].elems.push_back({ SPHERE, glm::vec3(x,y,z), newDirection, GetNextPos(x, y, z, SPHERE, newDirection), pipes[pipeNo].currentColor });
	} else {
		// nowhere to go.. generate new pipe
		glm::vec3 color{ (rand() % 100) / 100.0f , (rand() % 100) / 100.0f , (rand() % 100) / 100.0f };
		pipes[pipeNo].currentColor = color;

		std::vector<glm::vec3> availablePos;
		for (int i = 0; i < Lx; i++) {
			for (int j = 0; j < Ly; j++) {
				for (int k = 0; k < Lz; k++) {
					if (intersections[i][j][k] == 0) {
						availablePos.push_back(glm::vec3(i, j, k));
					}
				}
			}
		}
		if (availablePos.size() > 1) {
			intersections[x][y][z] = 1;
			pipes[pipeNo].spheres.push_back({ SPHERE, glm::vec3(x,y,z), pipes[pipeNo].elems.back().direction,glm::vec3(x,y,z), pipes[pipeNo].currentColor });
			pipes[pipeNo].elems.push_back({ SPHERE, glm::vec3(x,y,z), pipes[pipeNo].elems.back().direction, glm::vec3(x,y,z), pipes[pipeNo].currentColor });
			glm::vec3 newPos = availablePos[rand() % availablePos.size()];
			std::cout << "new root inserted --> " << newPos[0] << ", " << newPos[1] << ", " << newPos[2] << ", " << pipes[pipeNo].elems.size() << std::endl;
			roots.push_back(glm::vec3(newPos[0], newPos[1], newPos[2]));
			AddSphere(pipeNo, newPos[0], newPos[1], newPos[2]);
		}
		else if (availablePos.size() == 1) {
			intersections[x][y][z] = 1;
			pipes[pipeNo].spheres.push_back({ SPHERE, glm::vec3(x,y,z), pipes[pipeNo].elems.back().direction,glm::vec3(x,y,z), pipes[pipeNo].currentColor });
			pipes[pipeNo].elems.push_back({ SPHERE, glm::vec3(x,y,z), pipes[pipeNo].elems.back().direction, glm::vec3(x,y,z), pipes[pipeNo].currentColor });
			glm::vec3 newPos = availablePos[0];
			std::cout << "new root inserted --> " << newPos[0] << ", " << newPos[1] << ", " << newPos[2] << ", " << pipes[pipeNo].elems.size() << std::endl;
			roots.push_back(glm::vec3(newPos[0], newPos[1], newPos[2]));
			AddSphere(pipeNo, newPos[0], newPos[1], newPos[2]);
		} else {
			std::cout << "no more space" << std::endl;
			hasSpace = 0;
		}
	}

}

void CMyApp::AddCylinder(int pipeNo, int x, int y, int z) {
	Direction d = pipes[pipeNo].elems.back().direction;
	glm::vec3 nextPos = GetNextPos(x, y, z, CYLINDER, d);
	edges[x][y][z] = 1;
	pipes[pipeNo].cylinders.push_back({ CYLINDER, glm::vec3(x,y,z), d, nextPos, pipes[pipeNo].currentColor });
	pipes[pipeNo].elems.push_back({ CYLINDER, glm::vec3(x,y,z), d, nextPos, pipes[pipeNo].currentColor });
}

void CMyApp::AddNewElement(int pipeNo) {
	glm::vec3 nextPos = pipes[pipeNo].elems.back().nextElemPos;
	
	if (pipes[pipeNo].elems.back().shape == SPHERE) {
		AddCylinder(pipeNo, nextPos[0], nextPos[1], nextPos[2]);
	}
	else {
		if (rand() % 4) {
			std::vector<Direction> freeDirections = GetFreeDirections(nextPos[0], nextPos[1], nextPos[2]);
			std::vector<Direction>::iterator it = std::find(freeDirections.begin(), freeDirections.end(), pipes[pipeNo].cylinders.back().direction);

			if (it != freeDirections.end()) {
				int x1 = nextPos[0];
				int y1 = nextPos[1];
				int z1 = nextPos[2];
				if (pipes[pipeNo].elems.back().direction == DOWN)
					y1 += 1;
				if (pipes[pipeNo].elems.back().direction == LEFT)
					x1 += 1;
				if (pipes[pipeNo].elems.back().direction == BACKWARD)
					z1 += 1;
				intersections[x1][y1][z1] = 1;
				AddCylinder(pipeNo, nextPos[0], nextPos[1], nextPos[2]);
			}
			else {
				AddSphere(pipeNo, nextPos[0], nextPos[1], nextPos[2]);
			}
		}
		else {
			AddSphere(pipeNo, nextPos[0], nextPos[1], nextPos[2]);
		}
	}
}

glm::mat4 CMyApp::GetDirectionTransformation(Direction d) {
	switch (d) {
	case UP:
		return glm::rotate(float(0.0), glm::vec3(0, 0, 1));
		break;
	case DOWN:
		return glm::rotate(float(0.0), glm::vec3(0, 0, 1));
		break;
	case LEFT:
		return glm::rotate((float)(M_PI / 2.0), glm::vec3(0, 0, -1));
		break;
	case RIGHT:
		return glm::rotate((float)(M_PI / 2.0), glm::vec3(0, 0, -1));
		break;
	case FORWARD:
		return glm::rotate((float)(M_PI / 2.0), glm::vec3(1, 0, 0));
		break;
	case BACKWARD:
		return glm::rotate((float)(M_PI / 2.0), glm::vec3(1, 0, 0));
		break;
	}
}

void CMyApp::ResetButtonPressed() {
	std::cout << "Reset pressed" << std::endl;
	
	glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	roots.clear();

	if (Lx < 2) {
		Lx = 2;
		std::cout << "Lx should be at least 2" << std::endl;
	}
	if (Ly < 2) {
		Ly = 2;
		std::cout << "Ly should be at least 2" << std::endl;
	}
	if (Lz < 2) {
		Lz = 2;
		std::cout << "Lz should be at least 2" << std::endl;
	}

	for (int i = 0; i < nrPipes; i++) {
		pipes[i].elems.clear();
		pipes[i].cylinders.clear();
		pipes[i].spheres.clear();

		int x = (rand() % Lx) + 1;
		int y = (rand() % Ly) + 1;
		int z = (rand() % Lz) + 1;
		std::cout << x << ", " << y << ", " << z << ", " << std::endl;
		AddSphere(i, x, y, z);
	}
}

void CMyApp::ChangeFieldSize() {
	Lx = tmpSizes[0];
	Ly = tmpSizes[1];
	Lz = tmpSizes[2];

	edges = new int** [Lx + 2];
	for (int i = 0; i < Lx + 2; i++) {
		edges[i] = new int* [Ly + 2];
		for (int j = 0; j < Ly + 2; j++) {
			edges[i][j] = new int[Lz + 2];
			for (int k = 0; k < Lz + 2; k++) {
				if (i == 0 || i >= Lx || j == 0 || j >= Ly || k == 0 || k >= Lz) {
					edges[i][j][k] = 1;
				}
				else {
					edges[i][j][k] = 0;
				}
				std::cout << "edges init-->" << i << ", " << j << ", " << k << ", " << edges[i][j][k] << std::endl;
			}
		}
	}

	intersections = new int** [Lx + 2];
	for (int i = 0; i < Lx + 2; i++) {
		intersections[i] = new int* [Ly + 2];
		for (int j = 0; j < Ly + 2; j++) {
			intersections[i][j] = new int[Lz + 2];
			for (int k = 0; k < Lz + 2; k++) {
				if (i == 0 || i > Lx || j == 0 || j > Ly || k == 0 || k > Lz) {
					intersections[i][j][k] = 1;
				}
				else {
					intersections[i][j][k] = 0;
				}
				std::cout << "intersections init-->" << i << ", " << j << ", " << k << ", " << edges[i][j][k] << std::endl;
			}
		}
	}
}

bool CMyApp::Init()
{
	m_cameraMoved = false;

	edges = new int** [Lx + 2];
	for (int i = 0; i < Lx + 2; i++) {
		edges[i] = new int* [Ly + 2];
		for (int j = 0; j < Ly + 2; j++) {
			edges[i][j] = new int[Lz + 2];
			for (int k = 0; k < Lz + 2; k++) {
				if (i == 0 || i >= Lx || j == 0 || j >= Ly || k == 0 || k >= Lz) {
					edges[i][j][k] = 1;
				}
				else {
					edges[i][j][k] = 0;
				}
				std::cout << "edges init-->" << i << ", " << j << ", " << k << ", " << edges[i][j][k] << std::endl;
			}
		}
	}

	intersections = new int** [Lx + 2];
	for (int i = 0; i < Lx + 2; i++) {
		intersections[i] = new int* [Ly + 2];
		for (int j = 0; j < Ly + 2; j++) {
			intersections[i][j] = new int[Lz + 2];
			for (int k = 0; k < Lz + 2; k++) {
				if (i == 0 || i > Lx || j == 0 || j > Ly || k == 0 || k > Lz) {
					intersections[i][j][k] = 1;
				}
				else {
					intersections[i][j][k] = 0;
				}
				std::cout << "intersections init-->" << i << ", " << j << ", " << k << ", " <<  edges[i][j][k] << std::endl;
			}
		}
	}

	pipes = new Pipe[nrPipes];

	for (int i = 0; i < nrPipes; i++) {
		glm::vec3 color{ (rand() % 100) / 100.0f , (rand() % 100) / 100.0f , (rand() % 100) / 100.0f };
		pipes[i].currentColor = color;
		int x = (rand() % Lx) + 1;
		int y = (rand() % Ly) + 1;
		int z = (rand() % Lz) + 1;
		std::cout << x << ", " << y << ", " << z << ", " << std::endl;
		AddSphere(i, x, y, z);
	}

	// törlési szín legyen kékes
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

	glEnable(GL_CULL_FACE); // kapcsoljuk be a hatrafele nezo lapok eldobasat
	glEnable(GL_DEPTH_TEST); // mélységi teszt bekapcsolása (takarás)

	InitFrameBuffer(screenWidth, screenHeight);

	InitShaders();
	InitSphere();
	InitCylinder();
	InitSkyBox();

	// egyéb textúrák betöltése
	m_pipeTexture.FromFile("assets/pipe1.jpg");
	
	// kamera
	m_camera.SetProj(glm::radians(60.0f), 640.0f / 480.0f, 0.01f, 1000.0f);

	return true;
}

void CMyApp::Clean()
{
	if (m_frameBufferCreated)
	{
		glDeleteRenderbuffers(1, &m_depthBuffer);
		glDeleteTextures(1, &m_colorBuffer);
		glDeleteFramebuffers(1, &m_frameBuffer);
		m_frameBufferCreated = false;
	}
}

void CMyApp::Update()
{
	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	for (int i = 0; i < nrPipes; i++) {
		if (hasSpace) {
			AddNewElement(i);
		}
	}

	m_camera.Update(delta_time);


	last_time = SDL_GetTicks();
}

void CMyApp::Render()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_frameBuffer);
	//glEnable(GL_DEPTH_TEST);

	// töröljük a frampuffert (GL_COLOR_BUFFER_BIT) és a mélységi Z puffert (GL_DEPTH_BUFFER_BIT)
	//glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	
	if (m_cameraMoved) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glm::mat4 viewProj = m_camera.GetViewProj();

		int colorLocation = glGetUniformLocation(m_program, "color");
		m_program.Use();

		// pipes

		m_program.SetTexture("texImage", 0, m_pipeTexture);
		glm::mat4 pipeWorld;

		m_SphereVao.Bind();
		for (int i = 0; i < nrPipes; i++) {
			for (int j = 0; j < pipes[i].spheres.size(); j++) {
				glUniform4f(colorLocation, pipes[i].spheres[j].color[0], pipes[i].spheres[j].color[1], pipes[i].spheres[j].color[2], 1.0f);
				pipeWorld = glm::translate(pipes[i].spheres[j].pos);
				m_program.SetUniform("MVP", viewProj * pipeWorld);
				m_program.SetUniform("world", pipeWorld);
				m_program.SetUniform("worldIT", glm::inverse(glm::transpose(pipeWorld)));
				glDrawElements(GL_TRIANGLES, M * N * 3 * 2, GL_UNSIGNED_INT, nullptr);
			}
		}
		
		m_SphereVao.Unbind();

		m_CylinderVao.Bind();
		for (int i = 0; i < nrPipes; i++) {
			for (int j = 0; j < pipes[i].cylinders.size(); j++) {
				glUniform4f(colorLocation, pipes[i].cylinders[j].color[0], pipes[i].cylinders[j].color[1], pipes[i].cylinders[j].color[2], 1.0f);
				pipeWorld = glm::translate(pipes[i].cylinders[j].pos);
				if (pipes[i].cylinders[j].direction != UP && pipes[i].cylinders[j].direction != DOWN) {
					pipeWorld = pipeWorld * GetDirectionTransformation(pipes[i].cylinders[j].direction);
				}
				m_program.SetUniform("MVP", viewProj * pipeWorld);
				m_program.SetUniform("world", pipeWorld);
				m_program.SetUniform("worldIT", glm::inverse(glm::transpose(pipeWorld)));
				glDrawElements(GL_TRIANGLES, M * N * 3 * 2, GL_UNSIGNED_INT, nullptr);
			}
		}
		m_SphereVao.Unbind();
		// skybox
		// mentsük el az előző Z-test eredményt, azaz azt a relációt, ami alapján update-eljük a pixelt.
		GLint prevDepthFnc;
		glGetIntegerv(GL_DEPTH_FUNC, &prevDepthFnc);

		// most kisebb-egyenlőt használjunk, mert mindent kitolunk a távoli vágósíkokra
		glDepthFunc(GL_LEQUAL);

		m_SkyboxVao.Bind();
		m_programSkybox.Use();
		m_programSkybox.SetUniform("MVP", viewProj * glm::translate(m_camera.GetEye()));

		// cube map textúra beállítása 0-ás mintavételezőre és annak a shaderre beállítása
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);
		glUniform1i(m_programSkybox.GetLocation("skyboxTexture"), 0);
		// az előző három sor <=> m_programSkybox.SetCubeTexture("skyboxTexture", 0, m_skyboxTexture);

		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
		m_programSkybox.Unuse();

		// végül állítsuk vissza
		glDepthFunc(prevDepthFnc);

		m_cameraMoved = false;
	}
	else {
		glm::mat4 viewProj = m_camera.GetViewProj();

		int colorLocation = glGetUniformLocation(m_program, "color");
		m_program.Use();
		glUniform4f(colorLocation, 0.0f, 1.0f, 1.0f, 1.0f);

		// pipes

		m_program.SetTexture("texImage", 0, m_pipeTexture);
		glm::mat4 pipeWorld;

		m_SphereVao.Bind();
		for (int i = 0; i < nrPipes; i++) {
			glUniform4f(colorLocation, pipes[i].spheres.back().color[0], pipes[i].spheres.back().color[1], pipes[i].spheres.back().color[2], 1.0f);
			pipeWorld = glm::translate(pipes[i].spheres.back().pos);
			m_program.SetUniform("MVP", viewProj * pipeWorld);
			m_program.SetUniform("world", pipeWorld);
			m_program.SetUniform("worldIT", glm::inverse(glm::transpose(pipeWorld)));
			glDrawElements(GL_TRIANGLES, M * N * 3 * 2, GL_UNSIGNED_INT, nullptr);
		}
		m_SphereVao.Unbind();

		m_CylinderVao.Bind();
		for (int i = 0; i < nrPipes; i++) {
			glUniform4f(colorLocation, pipes[i].cylinders.back().color[0], pipes[i].cylinders.back().color[1], pipes[i].cylinders.back().color[2], 1.0f);
			pipeWorld = glm::translate(pipes[i].cylinders.back().pos);
			if (pipes[i].cylinders.back().direction != UP && pipes[i].cylinders.back().direction != DOWN) {
				pipeWorld = pipeWorld * GetDirectionTransformation(pipes[i].cylinders.back().direction);
			}
			m_program.SetUniform("MVP", viewProj * pipeWorld);
			m_program.SetUniform("world", pipeWorld);
			m_program.SetUniform("worldIT", glm::inverse(glm::transpose(pipeWorld)));
			glDrawElements(GL_TRIANGLES, M * N * 3 * 2, GL_UNSIGNED_INT, nullptr);
		}
		m_SphereVao.Unbind();

		// skybox
		// mentsük el az előző Z-test eredményt, azaz azt a relációt, ami alapján update-eljük a pixelt.
		GLint prevDepthFnc;
		glGetIntegerv(GL_DEPTH_FUNC, &prevDepthFnc);

		// most kisebb-egyenlőt használjunk, mert mindent kitolunk a távoli vágósíkokra
		glDepthFunc(GL_LEQUAL);

		m_SkyboxVao.Bind();
		m_programSkybox.Use();
		m_programSkybox.SetUniform("MVP", viewProj * glm::translate(m_camera.GetEye()));

		// cube map textúra beállítása 0-ás mintavételezőre és annak a shaderre beállítása
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, m_skyboxTexture);
		glUniform1i(m_programSkybox.GetLocation("skyboxTexture"), 0);
		// az előző három sor <=> m_programSkybox.SetCubeTexture("skyboxTexture", 0, m_skyboxTexture);

		glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, nullptr);
		m_programSkybox.Unuse();

		// végül állítsuk vissza
		glDepthFunc(prevDepthFnc);
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glDisable(GL_DEPTH_TEST); // disable depth test so screen-space quad isn't discarded due to depth test.
		// clear all relevant buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_postprocess.Use();
	m_postprocess.SetTexture("frameTex", 0, m_colorBuffer);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	//ImGui Testwindow
	ImGui::ShowTestWindow();

	ImGui::SetNextWindowPos(ImVec2(300, 400), ImGuiSetCond_FirstUseEver);
	ImGui::Begin("Settings");
	ImGui::InputInt3("Sizes", tmpSizes);
	if (ImGui::Button("Reset")) {
		if (Lx != tmpSizes[0] || Ly != tmpSizes[1] || Lz != tmpSizes[2]) {
			ChangeFieldSize();
		}
		ResetButtonPressed();
	}
	ImGui::End();
}

void CMyApp::KeyboardDown(SDL_KeyboardEvent& key)
{
	m_cameraMoved = true;
	m_camera.KeyboardDown(key);
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key)
{
	m_cameraMoved = true;
	m_camera.KeyboardUp(key);
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse)
{
	m_cameraMoved = true;
	m_camera.MouseMove(mouse);
}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse)
{
	m_cameraMoved = true;
}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse)
{
	m_cameraMoved = true;
}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel)
{
	m_cameraMoved = true;
}

// a két paraméterben az új ablakméret szélessége (_w) és magassága (_h) található
void CMyApp::Resize(int _w, int _h)
{
	m_cameraMoved = true;

	screenWidth = _w;
	screenHeight = _h;

	glViewport(0, 0, _w, _h );

	m_camera.Resize(_w, _h);

	if (m_frameBufferCreated)
	{
		glDeleteRenderbuffers(1, &m_depthBuffer);
		glDeleteTextures(1, &m_colorBuffer);
		glDeleteFramebuffers(1, &m_frameBuffer);
	}
	InitFrameBuffer(_w, _h);
}
