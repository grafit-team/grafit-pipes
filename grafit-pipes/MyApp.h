﻿#pragma once

// C++ includes
#include <memory>

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "includes/gCamera.h"

#include "includes/ProgramObject.h"
#include "includes/BufferObject.h"
#include "includes/VertexArrayObject.h"
#include "includes/TextureObject.h"

// mesh
#include "includes/ObjParser_OGL3.h"

class CMyApp
{
public:
	CMyApp();
	~CMyApp();

	bool Init();
	void Clean();

	void Update();
	void Render();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);

protected:
	// shaderekhez szükséges változók
	ProgramObject		m_program;			// mesh shader
	ProgramObject		m_programSkybox;	// skybox shader
	ProgramObject		m_postprocess;

	VertexArrayObject	m_SphereVao;			// VAO
	IndexBuffer			m_SphereIndices;		// index buffer
	ArrayBuffer			m_SphereVertexBuffer;	// VBO

	VertexArrayObject	m_CylinderVao;			// VAO
	IndexBuffer			m_CylinderIndices;		// index buffer
	ArrayBuffer			m_CylinderVertexBuffer;	// VBO

	VertexArrayObject	m_SkyboxVao;
	IndexBuffer			m_SkyboxIndices;	
	ArrayBuffer			m_SkyboxPos;		

	bool				m_frameBufferCreated{ false };
	GLuint				m_depthBuffer;
	GLuint				m_colorBuffer;
	GLuint				m_frameBuffer;

	gCamera				m_camera;

	Texture2D			m_pipeTexture;
	TextureCubeMap		m_skyboxTexture;

	bool				m_cameraMoved;


	struct Vertex
	{
		glm::vec3 p;
		glm::vec3 n;
		glm::vec2 t;
	};

	// a jobb olvashatóság kedvéért
	void InitShaders();
	void InitSphere();
	void InitCylinder();
	void InitSkyBox();
	void InitFrameBuffer(int width, int height);

	glm::vec3 GetSphereUV(float u, float v, float r);
	glm::vec3 GetSphereNormals(float u, float v);

	glm::vec3 GetCylinderUV(float u, float h, float r);
	glm::vec3 GetCylinderNormals(float u, float v);

	void AddNewElement(int pipeNo);

	void AddSphere(int pipeNo, int x, int y, int z);
	void AddCylinder(int pipeNo, int x, int y, int z);

	enum Direction {
		UP,
		DOWN,
		LEFT,
		RIGHT,
		FORWARD,
		BACKWARD
	};

	enum Shape {
		SPHERE,
		CYLINDER
	};

	struct Element
	{
		Shape shape;
		glm::vec3 pos;
		Direction direction;
		glm::vec3 nextElemPos;
		glm::vec3 color;
	};

	struct Pipe {
		std::vector<Element> cylinders;
		std::vector<Element> spheres;
		std::vector<Element> elems;

		glm::vec3 currentColor;
	}* pipes;
	
	std::vector<glm::vec3> roots;

	//std::vector<Direction> freeDirections;

	std::vector<Direction> GetFreeDirections(int x, int y, int z);

	glm::mat4 GetDirectionTransformation(Direction d);
	glm::vec3 GetNextPos(int x, int y, int z, Shape s, Direction d);

	// parametrikus feluletek reszletessege
	int N = 40, M = 40;

	// a kocka hatarai
	int Lx = 2, Ly = 2, Lz = 2;

	int screenWidth = 640;
	int screenHeight = 480;

	int*** edges;
	int*** intersections;

	int hasSpace = 1;

	int tmpSizes[3] = { 0 };

	int nrPipes = 3;
	
	void ResetButtonPressed();
	void ChangeFieldSize();

};

